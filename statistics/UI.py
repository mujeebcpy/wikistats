import logging
from telegram import ReplyKeyboardMarkup, InlineKeyboardMarkup
from telegram import InlineKeyboardButton
from telegram import CallbackQuery
from .XOPS import Xops


class UI:
	def __init__(self):
		logging.basicConfig(
				format='%(asctime)s %(message)s',
				datefmt='%m/%d/%Y %I:%M:%S %p')
		(self.FINPUT, self.USER, self.PAGE) = range(3)

	def start(self, bot, update):
		intro_file_handle = open('info/intro.txt')
		intro_text = intro_file_handle.read()
		bot.sendMessage(
			chat_id=update.message.chat_id,
			text=intro_text)
		keyboard = [[InlineKeyboardButton("usercount", callback_data='userc')],
		[InlineKeyboardButton("pagecount", callback_data='pagec')]]
		reply_markup = InlineKeyboardMarkup(keyboard)
		update.message.reply_text(
			"select from options",
			parse_mode='Markdown',
			reply_markup=reply_markup)
		return self.FINPUT
      
	def givedetails_user(self, bot, update):
		bot.sendMessage(
			chat_id=update.callback_query.message.chat_id,
			text="Enter language and username seperated by comma")
		return self.USER
		
	def preparing_user(self, bot, update):
		input_str = update.message.text
		lan, username = input_str.split(",")
		lann = Xops.getusercount(lan, username)
		bot.sendMessage(
			chat_id=update.message.chat_id,
			text="-> Edit count is {} executed in {} seconeds".format(lann[0],lann[1]))
		return self.FINPUT		
	
	def givedetails_page(self, bot, update):
		bot.sendMessage(
			chat_id=update.callback_query.message.chat_id,
			text="Enter the article link use @wiki to search english articles")
		return self.PAGE
		
	def preparing_page(self, bot, update):
		input_str = update.message.text
		splitted_url= input_str.split("/")
		lan = splitted_url[2]+"/"
		print (lan)
		pagename = splitted_url[4]
		print(pagename)
		lann = Xops.getpage(lan, pagename)
		bot.sendMessage(
			chat_id=update.message.chat_id,
			text="Watchers -> {} \nPageviews -> {} \nEditors -> {} \nauthor -> {}".format(lann[0],lann[1],lann[2],lann[3]))
		return self.FINPUT	
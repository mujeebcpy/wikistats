import urllib3
import json
import certifi
from urllib.parse import quote

class Xops:
	
	def __init__(self, url):
		self.url = url
		
	def basicop(lan, username, url):
		project = lan+".wikipedia.org/"
		final_url = url+project+username
		https= urllib3.PoolManager(cert_reqs='CERT_REQUIRED',ca_certs=certifi.where())
		r = https.request('GET', final_url)
		json_string = json.loads(r.data)
		return json_string
		
	def getusercount(lan, username):
		project = lan+".wikipedia.org/"
		url = 'https://xtools.wmflabs.org/api/user/simple_editcount/'
		final_url = url+project+username
		https= urllib3.PoolManager(cert_reqs='CERT_REQUIRED',ca_certs=certifi.where())
		r = https.request('GET', final_url)
		json_string = json.loads(r.data)
		edit_count = json_string['liveEditCount']
		eclapsed_time = json_string['elapsed_time']
		return edit_count, eclapsed_time
		
	def getpage(lan, pagename):
		url = 'https://xtools.wmflabs.org/api/page/articleinfo/'
		final_url = url+lan+quote(pagename)
		print(final_url)
		https= urllib3.PoolManager(cert_reqs='CERT_REQUIRED',ca_certs=certifi.where())
		r = https.request('GET', final_url)
		json_string = json.loads(r.data.decode('utf-8'))
		full_list = [json_string['watchers'], json_string['pageviews'], json_string['editors'], json_string['author']]
		return full_list

		
	
	

#project = 'ml.wikipedia.org/'
#username = 'mujeebcpy'


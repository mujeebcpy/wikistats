import telegram
import logging
import sys
from telegram.ext import Updater
from telegram.error import (
	TelegramError,
	Unauthorized,
	BadRequest,
	TimedOut,
	ChatMigrated,
	NetworkError)
from telegram.ext import CommandHandler, ConversationHandler
from telegram.ext import MessageHandler, Filters, RegexHandler, CallbackQueryHandler
from statistics import UI
#from statistics import Xops




def error_callback(bot, update, error):
	try:
		raise error
	except Unauthorized as e:
		print(e)
		# remove update.message.chat_id from
		# conversation list
	except BadRequest as e:
		print(e)
		# handle malformed requests
		# read more below!
	except TimedOut as e:
		print(e)
		# handle slow connection problems
	except NetworkError as e:
		print(e)
		# handle other connection problems
	except ChatMigrated as e:
		print(e)
		# the chat_id of a group has changed, use e.new_chat_id instead
	except TelegramError as e:
		print(e)
		# handle all other telegram related errors


def cancel(bot, update):
	# Cancel message here
	update.message.reply_text('Bye! I hope we can talk again some day.')
	return ConversationHandler.END
    

def main():
	#xmlops = XMLOps()
	#Channel = UI(xmlops)
	statui = UI()
	TOKEN = sys.argv[1]
	updater = Updater(token=TOKEN)
	bot = telegram.Bot(token=TOKEN)
	print(bot.getMe())
	dispatcher = updater.dispatcher
	logging.basicConfig(
		format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
		level=logging.INFO)
	dispatcher.add_error_handler(error_callback)
	statusui_handler =  ConversationHandler(
		entry_points=[CommandHandler('start', statui.start)],
		states={
			statui.FINPUT: [CallbackQueryHandler(statui.givedetails_user, pattern=r'userc'),
						CallbackQueryHandler(statui.givedetails_page, pattern=r'pagec')
						],
			statui.USER: [MessageHandler(Filters.text, statui.preparing_user)],
			statui.PAGE: [MessageHandler(Filters.text, statui.preparing_page)]
                     
            
			
		},
		fallbacks=[CommandHandler('cancel', cancel)]
	)
	dispatcher.add_handler(statusui_handler)
	updater.start_polling()
	updater.idle()

if __name__ == "__main__":
    main()
